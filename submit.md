# Qu'est ce que votre projet ?

### Description succincte du projet

...

### Quel est l'objectif principal de celui-ci ?

...

# A qui sert-il ?

### Quels sont les différents types d'utilisateurs ?

...

# Pourquoi ce projet ?

### Comment se positionne votre projet par rapport à l'existant ?

...

### Quels sont les concurrents (Descriptions succinctes) ?

...

### Qu'apporte votre projet ? Y a-t-il un manque ? Un besoin nouveau ?

...

### A quels problèmes ou besoins votre projet répond-t-il ?

...

### Que feront vos utilisateurs avec votre projet ?

...

#  Décrire le résultat

### Comment utiliser votre projet ?

...

### Comment interagir avec le projet ?

...

### Quelles sont les interactions de votre produit par les utilisateurs ?

...

### A quoi ressemblera le produit fini ?

...

### Sur quoi cela fonctionnera-t-il ?

...

# Moyens : avec quoi ?

### Quels moyens humains ?

...

### Des idées de partenaire ?

...

### De quels moyens matériels auriez-vous besoin ?

...


### Quels sont les logiciels dont vous auriez besoin ?

...

# Comment vous le fabriquez ?

### Qu'est ce que vous développez ?

...

# Quand ?

### Quel est votre planning ?

...

# Où ?

### Devez-vous être ou interagir avec des lieux particuliers pour fabriquer votre projet ?

...

# Contraintes

### Quelles sont les types de contraintes ?

...

# Périmètre de votre projet

### Quels sont les chiffres associés à votre projet ?

...

# Cadre légal de votre projet

### Vérifier les aspects légaux : données utilisateurs, droits d'auteurs, contrefaçons... Voyez-vous un potentiel problème ?

...

# Cadre du projet

### Votre projet est-il une reprise ou contribution à un projet existant, un projet d'entreprise (création ou commandité...) ?

...
